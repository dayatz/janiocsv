import csv

def split_csv(file_name, row_limit=3000):
    """
    Splits a CSV file into multiple files with a given row limit.

    :param file_name: The name of the CSV file to split.
    :param row_limit: The maximum number of rows in each split file.
    """
    with open(file_name, 'r', newline='') as file:
        reader = csv.reader(file)
        current_file_index = 1
        current_file = open(f"{file_name}_part_{current_file_index}.csv", 'w', newline='')
        writer = csv.writer(current_file)

        for i, row in enumerate(reader):
            if i >= row_limit * current_file_index:
                current_file.close()
                current_file_index += 1
                current_file = open(f"{file_name}_part_{current_file_index}.csv", 'w', newline='')
                writer = csv.writer(current_file)
            
            writer.writerow(row)

        current_file.close()

# Example usage:
split_csv("cainiao_push.csv")

# This script will read 'your_file.csv' and create multiple files named 'your_file_part_1.csv', 'your_file_part_2.csv', etc.,
# each with up to 3000 rows.

